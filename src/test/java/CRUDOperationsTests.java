import net.project.hibernate.CRUDOperations;
import net.project.hibernate.entity.Student;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class CRUDOperationsTests {

    private static CRUDOperations crud;

    @BeforeAll
    public static void setUp() {
        crud = new CRUDOperations();
    }

    @Test
    public void testInsertEntity() {
        //GIVEN
        Student student = new Student("Maciej", "Nowak", "mnowak@gmail.com");

        //WHEN
        crud.insertEntity(student);

        //THEN
        Student found = crud.findEntity(student);
        assertEquals(found, student);
    }

    @Test
    public void testUpdateEntity() {
        //GIVEN
        Student student = new Student("Maciej", "Nowak", "mnowak@gmail.com");
        crud.insertEntity(student);
        student.setFirstName("Kuba");

        //WHEN
        crud.updateEntity(student);

        //THEN
        Student found = crud.findEntity(student);
        assertEquals("Kuba", found.getFirstName());
    }

    @Test
    public void testRemoveEntity() {
        //GIVEN
        Student student = new Student("Maciej", "Nowak", "mnowak@gmail.com");
        crud.insertEntity(student);

        //WHEN
        crud.removeEntity(student);

        //THEN
        Student found = crud.findEntity(student);
        assertNull(found);
    }
}

//szablon:
//GIVEN
//WHEN
//THEN
