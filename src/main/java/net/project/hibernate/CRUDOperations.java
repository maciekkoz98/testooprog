package net.project.hibernate;

import net.project.hibernate.entity.Student;
import net.project.hibernate.util.JPAUtil;

import javax.persistence.EntityManager;

public class CRUDOperations {

    public void insertEntity(Student student) {
        EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(student);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public Student findEntity(Student toBeFound) {
        EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Student found = entityManager.find(Student.class, toBeFound.getID());
        entityManager.getTransaction().commit();
        entityManager.close();
        return found;
    }

    public void updateEntity(Student toBeUpdated) {
        EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Student oldStudent = entityManager.find(Student.class, toBeUpdated.getID());
        oldStudent.setFirstName(toBeUpdated.getFirstName());
        oldStudent.setLastName(toBeUpdated.getLastName());
        oldStudent.setEmail(toBeUpdated.getEmail());
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    public void removeEntity(Student toBeRemoved) {
        EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Student removingStudent = entityManager.find(Student.class, toBeRemoved.getID());
        entityManager.remove(removingStudent);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
