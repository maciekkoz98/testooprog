package net.project.hibernate;

import net.project.hibernate.entity.Student;

public class App {
    //Main app
    public static void main(String[] args) {
        CRUDOperations crud = new CRUDOperations();
        Student student = new Student("Adam", "Andruszkiewicz", "adrianek@gov.pl");
        crud.insertEntity(student);
        Student found;
        found = crud.findEntity(student);
        System.out.println("znaleziony student: " + found.toString());
        student.setFirstName("Maciej");
        crud.updateEntity(student);
        crud.removeEntity(student);
    }
}
